#pragma once
#include <iostream>
#include <vector>
#include <stack>
#include <queue>
using namespace std;

template <typename T>
class RedBlackTree
{
	/*public:
		typedef enum { BST_OK,
					   BST_OUTOFMEMORY,
					   BST_ALREADYEXISTS,
					   BST_DOESNTEXIST,
					   BST_FAILED,
					   BST_EMPTY } BST_ERROR;*/

 private:
	class Node
	{
	public:
		T     Data;
        bool Colour; //1 for black and 0 for red
		Node* Left;
		Node* Right;
		
		Node(const T& Item)
			:Data(Item), Left(nullptr), Right(nullptr),Colour(0) {/**/
		}//default constructor

		Node(const T& Item, Node* L, Node* R) noexcept
			:Data(Item), Left(L), Right(R),Colour(0) {/**/
		}//constructor

		~Node() { Left = Right = nullptr; }//destructor

		//--- AUXILIARY METHODS ---//
		inline bool IsLeafNode() const noexcept
		{
			return Left == nullptr && Right == nullptr;
		} //	auxiliary methods

		inline bool HasOneChild() const noexcept
		{
			return (Left == nullptr && Right != nullptr) || (Left != nullptr && Right == nullptr);
		}

		inline bool HasTwoChildren() const noexcept
		{
			return (Left != nullptr && Right != nullptr);
		}
		//------------------------//

	};//nodeClass

	Node* Root;
	size_t NumNodes;

	//--- PRIVATE METHODS ---//
	inline bool DeleteTree(Node* node)noexcept;
	inline size_t Height(Node* node)const noexcept;
	inline size_t NodeCount(Node* node)const noexcept;
	inline void InOrder(Node* node)const noexcept;
	inline void PostOrder(Node* node)const noexcept;
	inline void PreOrder(Node* node)const noexcept;
	inline void LevelOrder(Node* node)const noexcept;
	
 public:

	//--- PUBLIC METHODS ---//
	RedBlackTree()noexcept;//Default Constructor
	~RedBlackTree()noexcept;//Destructor
	RedBlackTree(const RedBlackTree<T>& other) noexcept;//Copy Constructor
	RedBlackTree<T>& operator= (const RedBlackTree<T>& other)noexcept;//Operator=
	RedBlackTree(RedBlackTree<T>&& other)noexcept;//Move constructor
	RedBlackTree<T>& operator= (const RedBlackTree<T>&& other)noexcept;//move operator
	inline bool Add(const T& newData)noexcept;//Add a new node 
	inline bool Rotate(Node* node) noexcept;
	inline bool DeleteTree()noexcept;//Delete Tree
	inline bool Compare(const RedBlackTree<T>& other) const noexcept;//Compare two trees
	inline bool operator==(const RedBlackTree<T>& other) const noexcept;//Compare two trees
	inline bool isEmpty()const noexcept;//Checks if Tree is Empty
	inline bool SearchNode(const T& Value)const noexcept;//Searches if a Node Exists
	inline bool DeleteNode(const T& data)noexcept;//Deletes a node if it exists
	inline size_t GetItemCount()const noexcept;//Returns the number of nodes inside the tree
	inline size_t Height()const noexcept;//Returns the height of the tree
	inline size_t NodeCount()const noexcept;//This method counts the number of nodes 
	inline void PreOrder()const noexcept;
	inline void PostOrder()const noexcept;
	inline void InOrder()const noexcept;
	inline void LevelOrder()const noexcept;
	inline std::vector<T> ToArray() const noexcept; //from a vector to a BST
	inline bool FromArray(const std::vector<T>& v) noexcept; //from a BST to a vector
};//RedBlackTree

template<typename T>
RedBlackTree<T>::RedBlackTree()noexcept
// :Root(nullptr),NumNodes(0)
{
	Root = nullptr;
	NumNodes = 0;
}//Default Constructor

template<typename T>
RedBlackTree<T>::~RedBlackTree() { DeleteTree(); }//Destructor

template<typename T>
RedBlackTree<T>::RedBlackTree(const RedBlackTree<T>& other)noexcept
{
	if (other.isEmpty())return;
	else
	{
		stack<Node*> nodeStack;
		nodeStack.push(other.Root);

		while (nodeStack.empty() == false)
		{
			Node* node = nodeStack.top();
			Add(node->Data);
			nodeStack.pop();

			if (node->Right)nodeStack.push(node->Right);
			if (node->Left)nodeStack.push(node->Left);

		}//if
	}//if
}//copy constructor

template<typename T>
RedBlackTree<T>& RedBlackTree<T>::operator=(const RedBlackTree<T>& other)noexcept
{
	//RedBlackTree<T> Obj;
	if (other.isEmpty())return *this;//RedBlackTree<T> (Obj);
	else
	{
		stack<Node*> nodeStack;
		nodeStack.push(other.Root);

		while (nodeStack.empty() == false)
		{
			Node* node = nodeStack.top();

			/*Obj.*/Add(node->Data);

			nodeStack.pop();

			if (node->Right)nodeStack.push(node->Right);
			if (node->Left)nodeStack.push(node->Left);

		}//if
	}//if

	return *this;// RedBlackTree<T> (Obj);
}//operator=

template<typename T>
RedBlackTree<T>::RedBlackTree(RedBlackTree<T>&& other)noexcept
	:Root(other.Root), NumNodes(other.NumNodes)
{
	other.Root = nullptr;
	other.NumNodes = 0;

}//move constructor

template<typename T>
RedBlackTree<T>& RedBlackTree<T>::operator=(const RedBlackTree<T>&& other) noexcept
{
	if (this != &other)
	{
		DeleteTree();
		Root = other.Root;
		NumNodes = other.NumNodes;
		other.Root = nullptr;
		other.NumNodes = 0;
	}//if
}//move operator

template<typename T>
inline bool RedBlackTree<T>::Add(const T& newData)noexcept
{
	Node* newNode = new(std::nothrow) Node(newData);
	if (isEmpty()) 
	{
		Root = newNode; 
		Root->Colour = 1;
	}
	else
	{
		Node* Parent = nullptr;
		Node* tmp2 = nullptr;
		Node* tmp = Root;

		do
		{
			tmp2 = Parent;
			Parent = tmp;

			if (newData == Parent->Data) return false;
			else if (newData > Parent->Data) tmp = tmp->Right;
			else tmp = tmp->Left;

		} while (tmp != nullptr);

		if (newData > Parent->Data) { Parent->Right = newNode; }
		else { Parent->Left = newNode; }
        
	}//if
	NumNodes++;
	do 
	{
	   if (NumNodes >= 3) { Rotate(Root); }
	} while (Rotate(Root) == 0);
	return true;
}//Add

template<typename T>
inline bool RedBlackTree<T>::Rotate(Node* root) noexcept
{
	if (root != nullptr)
	{
		Node* tmp = root->Right;
		Node* tmp2 = root->Left;

		//===Case2===============================================================//
		if (tmp != nullptr)
		{
			if (tmp->Colour == 0)

				if (tmp->Right != nullptr)
					if (tmp->Right->Colour == 0)
						if (tmp2 != nullptr)
							if (tmp2->Colour == 0)
							{
								tmp2->Colour = 1;
								tmp->Colour = 1;
								if (root != Root) { root->Colour = 0; }
							}
		}
		if (tmp != nullptr)
		{
			if (tmp->Colour == 0)

				if (tmp->Left != nullptr)
					if (tmp->Left->Colour == 0)
						if (tmp2 != nullptr)
							if (tmp2->Colour == 0)
							{
								tmp2->Colour = 1;
								tmp->Colour = 1;
								if (root != Root) { root->Colour = 0; }
							}
		}//Right
		if (tmp2 != nullptr)
		{
			if (tmp2->Colour == 0)

				if (tmp2->Left != nullptr)
					if (tmp2->Left->Colour == 0)
						if (tmp != nullptr)
							if (tmp->Colour == 0)
							{
								tmp->Colour = 1;
								tmp2->Colour = 1;
								if (root != Root) { root->Colour = 0; }
							}

		}
		if (tmp2 != nullptr)
		{
			if (tmp2->Colour == 0)

				if (tmp2->Right != nullptr)
					if (tmp2->Right->Colour == 0)
						if (tmp != nullptr)
							if (tmp->Colour == 0)
							{
								tmp2->Colour = 1;
								tmp->Colour = 1;
								if (root != Root) { root->Colour = 0; }
							}
			//Left
		}
		//=======================================================================//
		//CASE 4=================================================================//
		if (tmp != nullptr)
		{
			if (tmp->Colour == 0)
				if (tmp->Right != nullptr)
					if (tmp->Right->Colour == 0)
						if ((tmp2 == nullptr) || (tmp2->Colour == 1))
						{

							Node* N = root->Right;
							Node* N2 = N->Left;

							std::swap(*root, *N);
							root->Left = N;
							N->Right = N2;

							root->Colour = 1;
							N->Colour = 0;
							

							return true;
                        }//LeftRotation
		}//if
		if (tmp2 != nullptr)
		{
			if (tmp2->Colour == 0)
				if (tmp2->Left != nullptr)
					if (tmp2->Left->Colour == 0)
						if ((tmp == nullptr) || (tmp->Colour == 1))
						{

							Node* N = root->Left;
							Node* N2 = N->Right;

							std::swap(*root, *N);
							root->Right = N;
							N->Left = N2;

							root->Colour = 1;
							N->Colour = 0;


							return true;
						}//if
		}//if
		//=======================================================================//
		//CASE 3=================================================================//
		if (tmp != nullptr)
		{
			if (tmp->Colour == 0)
				if (tmp->Left != nullptr)
					if (tmp->Left->Colour == 0)
						if ((tmp2 == nullptr) || (tmp2->Colour == 1))
						{
							if (root != nullptr)
							{
								Node* help = tmp2->Left;
								tmp = root->Right;
								tmp2 = tmp->Left;
								

								root->Right = tmp2;
								tmp2->Right = tmp;
								tmp->Left = help;

								tmp = root->Right;
								tmp2 = tmp->Left;

								std::swap(*root, *tmp);
								root->Left = tmp;
								tmp->Right = tmp2;

								root->Colour = 1;
								tmp->Colour = 0;				
							}//if
						}//if
		}//if
		if (tmp2 != nullptr)
		{
			if (tmp2->Colour == 0)
				if (tmp2->Right != nullptr)
					if (tmp2->Right->Colour == 0)
						if ((tmp == nullptr) || (tmp->Colour == 1))
						{
							if (root != nullptr)
							{
								Node* help = tmp2->Left;
								tmp = root->Left;
								tmp2 = tmp->Right;
								

								root->Left = tmp2;
								tmp2->Left = tmp;
								tmp->Right = help;

								tmp = root->Left;
								tmp2 = tmp->Right;

								std::swap(*root, *tmp);
								root->Right = tmp;
								tmp->Left = tmp2;
									
								root->Colour = 1;
								tmp->Colour = 0;

							}//if
						}//if
		}//if

        //=======================================================================//
		Rotate(root->Left);
		Rotate(root->Right);
		return true;
	}//if
	return false;
}//Rotate

template<typename T>
inline bool RedBlackTree<T>::DeleteTree(Node* node)noexcept
{
	if (node != nullptr)
	{
		DeleteTree(node->Left);
		DeleteTree(node->Right);
		delete node;
		Root = nullptr;
		NumNodes = 0;

		return true;
	}//if
	else return false;
}//DeleteTree

template<typename T>
inline bool RedBlackTree<T>::DeleteTree()noexcept
{
	if (!isEmpty()) return DeleteTree(Root);
	else return true;//false;
}//DeleteTree

template<typename T>
inline bool RedBlackTree<T>::Compare(const RedBlackTree<T>& other)const noexcept
{
	if (this->NumNodes != other.NumNodes)return false;

	vector<T> ArrayTree1;
	vector<T> ArrayTree2;
	ArrayTree1 = this->ToArray();
	ArrayTree2 = other.ToArray();


	for (size_t i = 0; i < ArrayTree1.size(); i++)
	{
		if (ArrayTree1[i] != ArrayTree2[i]) return false;
	}//for

	return true;
}//Compare

template<typename T>
inline bool RedBlackTree<T>::operator==(const RedBlackTree<T>& other) const noexcept
{
	if (this->NumNodes != other.NumNodes)return false;

	vector<T> ArrayTree1;
	vector<T> ArrayTree2;
	ArrayTree1 = this->ToArray();
	ArrayTree2 = other.ToArray();

	for (size_t i = 0; i < ArrayTree1.size(); i++)
	{
		if (ArrayTree1[i] != ArrayTree2[i]) return false;
	}//for

	return true;
}//operator==

template<typename T>
inline bool RedBlackTree<T>::isEmpty()const noexcept
{
	return(NumNodes == 0);
}//isEmpty

template<typename T>
inline bool RedBlackTree<T>::SearchNode(const T& Value)const noexcept
{
	if (isEmpty()) return false;
	else
	{
		if (Root->Data == Value) return true;
		else
		{
			Node* Parent = nullptr;
			Node* tmp = Root;
			while (tmp != nullptr)
			{
				Parent = tmp;
				if (Value == Parent->Data) return true;
				else if (Value > Parent->Data)tmp = tmp->Right;
				else tmp = tmp->Left;
			}//while
		}//if
	}//if
	return false;
}//Search Node

template<typename T>
inline bool RedBlackTree<T>::DeleteNode(const T& data)noexcept
{
	if (isEmpty())return false;
	else
	{
		Node* tmp = Root;
		Node* Parent = Root;
		Node* temp = Root;

		do
		{
			temp = Parent;
			Parent = tmp;
			if (data == tmp->Data)
			{
				if (tmp->IsLeafNode())
				{
					if (temp->Left == tmp)
					{
						delete tmp;
						temp->Left = nullptr;
						NumNodes--;
						do
						{
							Rotate(Root);
						} while (Rotate(Root) == 0);
						return true;
					}
					else if (temp->Right == tmp)
					{
						delete tmp;
						temp->Right = nullptr;
						NumNodes--;
						do
						{
							Rotate(Root);
						} while (Rotate(Root) == 0);
						return true;
					}
					else
					{
						DeleteTree();
						return true;
					}//if
				}//if

				if (tmp->HasOneChild())
				{
					if (temp->Left == tmp)
					{
						if (tmp->Left != nullptr)
						{
							temp->Left = tmp->Left;
							delete tmp;

							do
							{
								Rotate(Root);
							} while (Rotate(Root) == 0);

							NumNodes--;
							return true;
						}
						else if (tmp->Right != nullptr)
						{
							temp->Left = tmp->Right;
							delete tmp;
							NumNodes--;
							do
							{
								Rotate(Root);
							} while (Rotate(Root) == 0);

							return true;
						}//if
						else return false;
					}//if
					else if (temp->Right == tmp)
					{
						if (tmp->Left != nullptr)
						{
							temp->Right = tmp->Left;
							delete tmp;
							NumNodes--;

							do
							{
								Rotate(Root);
							} while (Rotate(Root) == 0);

							return true;
						}
						else if (tmp->Right != nullptr)
						{
							temp->Right = tmp->Right;
							delete tmp;
							NumNodes--;
							do
							{
								Rotate(Root);
							} while (Rotate(Root) == 0);

							return true;
						}//if
						else return false;
					}
					else if (tmp == temp)
					{
						if (tmp->Left != nullptr)
						{
							Root = tmp->Left;
							delete tmp;
							NumNodes--;
							do
							{
								Rotate(Root);
							} while (Rotate(Root) == 0);
							return true;
						}
						else
						{
							Root = tmp->Right;
							delete tmp;
							NumNodes--;
							do
							{
								Rotate(Root);
							} while (Rotate(Root) == 0);
							return true;
						}//if
					}//if
				}//if

				if (tmp->HasTwoChildren())
				{
					if (Parent->Data < Root->Data)
					{
						temp = Parent;
						tmp = Parent->Left;

						while (tmp->Right != nullptr)
						{
							temp = tmp;
							tmp = tmp->Right;
						}//while

						Parent->Data = tmp->Data;

						if (temp->Left == tmp)
						{

							temp->Left = tmp->Left;
						}
						else
						{
							temp->Right = tmp->Left;
						}

						delete tmp;
						NumNodes--;
						do
						{
							Rotate(Root);
						} while (Rotate(Root) == 0);
						return true;
					}
					else if (Parent->Data >= Root->Data)
					{
						temp = Parent;
						tmp = Parent->Right;

						while (tmp->Left != nullptr)
						{
							temp = tmp;
							tmp = tmp->Left;
						}//while

						Parent->Data = tmp->Data;

						if (temp->Right == tmp)
						{

							temp->Right = tmp->Right;
						}
						else
						{
							temp->Left = tmp->Right;
						}
						delete tmp;
						NumNodes--;
						do
						{
							Rotate(Root);
						} while (Rotate(Root) == 0);
						return true;
					}//if
					else return false;
				}//if

			}//if
			else if (data > tmp->Data)tmp = tmp->Right;
			else tmp = tmp->Left;
		} while (tmp != nullptr);
	}//if
	return false;
}//Delete Node

template<typename T>
inline size_t RedBlackTree<T>::GetItemCount()const noexcept
{
	return NumNodes;
}//GetItemCount

template<typename T>
inline size_t RedBlackTree<T>::Height()const noexcept
{
	return Height(Root);
}//Height

template<typename T>
inline size_t RedBlackTree<T>::Height(Node* node)const noexcept
{
	if (node == nullptr) return 0;

	size_t HeightLeft = Height(node->Left);
	size_t HeightRight = Height(node->Right);

	if (HeightLeft > HeightRight)return ++HeightLeft;
	else return ++HeightRight;
}

template<typename T>
inline size_t RedBlackTree<T>::NodeCount()const noexcept
{
	size_t count = 0;
	if (Root != nullptr) count = NodeCount(Root);

	return count;
}//Node Count

template<typename T>
inline size_t RedBlackTree<T>::NodeCount(Node* node)const noexcept
{
	size_t count = 1;
	if (node->Left != nullptr) count += NodeCount(node->Left);
	if (node->Right != nullptr)count += NodeCount(node->Right);
	return count;
}//Node Count

template<typename T>
inline void RedBlackTree<T>::PreOrder()const noexcept
{
	if (Root != nullptr) PreOrder(Root);
}//PreOrder

template<typename T>
inline void RedBlackTree<T>::PreOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		cout << node->Data << " ";
		PreOrder(node->Left);
		PreOrder(node->Right);
	}//if
}//PreOrder

template<typename T>
inline void RedBlackTree<T>::PostOrder()const noexcept
{
	if (Root != nullptr) PostOrder(Root);
}//PostOrder

template<typename T>
inline void RedBlackTree<T>::PostOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		PostOrder(node->Left);
		PostOrder(node->Right);
		cout << node->Data << " ";
	}//if
}//PostOrder

template<typename T>
inline void RedBlackTree<T>::InOrder()const noexcept
{
	if (Root != nullptr)InOrder(Root);
}//InOrder

template<typename T>
inline void RedBlackTree<T>::InOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		InOrder(node->Left);
		cout << node->Data << " ";
		InOrder(node->Right);
	}//if
}//InOrder

template<typename T>
inline void RedBlackTree<T>::LevelOrder() const noexcept
{
	if (!isEmpty()) { LevelOrder(Root); }
}//LevelOrder

template<typename T>
inline void RedBlackTree<T>::LevelOrder(Node* node) const noexcept
{
	queue<Node*> q;  
	Node* temp;
	q.push(Root);
	while (!q.empty()) {
		temp = q.front();
		q.pop();
		cout << temp->Data << " ";  
		if (temp->Left)
			q.push(temp->Left); 
		if (temp->Right)
			q.push(temp->Right); 
	}//while
}//LevelOrder

template<typename T>
inline std::vector<T> RedBlackTree<T>::ToArray() const noexcept
{
	vector<T> ArrayData;
	stack<Node*> nodeStack;

	nodeStack.push(Root);

	while (nodeStack.empty() == false)
	{
		Node* node = nodeStack.top();
		ArrayData.push_back(node->Data);

		nodeStack.pop();

		if (node->Right)nodeStack.push(node->Right);
		if (node->Left)nodeStack.push(node->Left);
	}
	return std::vector<T>(ArrayData);
}//ToArray

template<typename T>
inline bool RedBlackTree<T>::FromArray(const std::vector<T>& v) noexcept
{
	if (!v.empty())
	{
		for (size_t i = 0; i < v.size(); i++)
		{
			Add(v[i]);
		}//for 
		return true;
	}//if
	return false;
}//FromArray




